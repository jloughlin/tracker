#include "Wire.h"
#include "I2Cdev.h"
#include "MPU6050.h"
#include "math.h"
#include <NewPing.h>
#include "U8g2lib.h"

#define leftMotorPWMPin   6
#define leftMotorDirPin   7
#define rightMotorPWMPin  3
#define rightMotorDirPin  2

#define TRIGGER_PIN 9
#define ECHO_PIN 8
#define MAX_DISTANCE 75

float Kp = 30.0; // 40
float Ki = 0.0; // 40
float Kd = 0.0; // 0.05
#define sampleTime  0.005
#define targetAngle -2.5

//#define USE_DISPLAY
//#define SONAR

MPU6050 mpu;
#ifdef SONAR
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);
#endif

#ifdef USE_DISPLAY
U8G2_SH1106_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
#endif

int16_t accY, accZ, gyroX;
volatile int motorPower, gyroRate;
volatile float accAngle, gyroAngle, currentAngle, prevAngle=0, error, prevError=0, errorSum=0;
volatile byte count=0;
int distanceCm;

void setMotors(int leftMotorSpeed, int rightMotorSpeed) 
{
  if(leftMotorSpeed >= 0) {
    analogWrite(leftMotorPWMPin, leftMotorSpeed);
    digitalWrite(leftMotorDirPin, LOW);
  }
  else {
//    analogWrite(leftMotorPWMPin, 255 + leftMotorSpeed);
    analogWrite(leftMotorPWMPin, abs(leftMotorSpeed));
    digitalWrite(leftMotorDirPin, HIGH);
  }
  if(rightMotorSpeed >= 0) {
    analogWrite(rightMotorPWMPin, rightMotorSpeed);
    digitalWrite(rightMotorDirPin, LOW);
  }
  else {
//    analogWrite(rightMotorPWMPin, 255 + rightMotorSpeed);
    analogWrite(rightMotorPWMPin, abs(rightMotorSpeed));
    digitalWrite(rightMotorDirPin, HIGH);
  }
}

void init_PID()
{  
  // initialize Timer1
  cli();          // disable global interrupts
  TCCR1A = 0;     // set entire TCCR1A register to 0
  TCCR1B = 0;     // same for TCCR1B    
  // set compare match register to set sample time 5ms
  OCR1A = 9999;    
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS11 bit for prescaling by 8
  TCCR1B |= (1 << CS11);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  sei();          // enable global interrupts
}

void setup() 
{
  // set the motor control and PWM pins to output mode
  pinMode(leftMotorPWMPin, OUTPUT);
  pinMode(leftMotorDirPin, OUTPUT);
  pinMode(rightMotorPWMPin, OUTPUT);
  pinMode(rightMotorDirPin, OUTPUT);
  // set the status LED to output mode 
  pinMode(13, OUTPUT);
  // initialize the MPU6050 and set offset values
  //mpu.initialize();
  //mpu.setYAccelOffset(1593);
  //mpu.setZAccelOffset(963);
  //mpu.setXGyroOffset(40);
  // initialize PID sampling loop
  init_PID();
  Serial.begin(115200);

#ifdef USE_DISPLAY
  u8g2.begin();  
#endif
}

int GetSerialMsg(char* msg)
{
  int idx = 0;
  while(Serial.available())
  {
     char ch = Serial.read();
     if (ch == '\r' || ch == '\n')
     {
       msg[idx] = '\0';
       Serial.flush();
       break;
     }
     msg[idx++] = ch;
     delay(10);
  }  
  return idx;
}

long lastBeaconTime = 0;
int leftMotorSpeed = 0;
int rightMotorSpeed = 0;

void loop() 
{
#ifdef USE_DISPLAY
    u8g2.clearBuffer();          
    u8g2.setFont(u8g2_font_ncenB08_tr); 
    char val[32];
    char str[32];
    dtostrf(Kp, 4, 2, val);
    sprintf(str, "Kp:  %s", val);
    u8g2.drawStr(10, 10, str);

    dtostrf(Ki, 4, 2, val);
    sprintf(str, "Ki:  %s", val);
    u8g2.drawStr(10, 22, str);

    dtostrf(Kd, 4, 2, val);
    sprintf(str, "Kd:  %s", val);
    u8g2.drawStr(10, 34, str);
    u8g2.sendBuffer();
#endif

  // read acceleration and gyroscope values
  //accY = mpu.getAccelerationY();
  //accZ = mpu.getAccelerationZ();  
  //gyroX = mpu.getRotationX();
  // set motor power after constraining it
  //motorPower = constrain(motorPower, -255, 255);
  //setMotors(motorPower, motorPower);
#ifdef SONAR
  // measure distance every 100 milliseconds
  if((count%20) == 0){
    distanceCm = sonar.ping_cm();
  }
  if((distanceCm < 20) && (distanceCm != 0)) {
    setMotors(-motorPower, motorPower);
  }
#endif

  char msg[256] = {0};
  int len = GetSerialMsg(msg);

  if (strlen(msg) > 0)
  {
    if (msg[0] == 'M' || msg[0] == 'm')
    {  
        // Motor command
        const char *ptr = strtok(msg + 1, ",");
        leftMotorSpeed = atoi(ptr);
        ptr = strtok(0, ",");
        rightMotorSpeed = atoi(ptr);

        char outStr[64];
        sprintf(outStr, "%d,%d", leftMotorSpeed, rightMotorSpeed);
        Serial.println(outStr);
        lastBeaconTime = millis();
        setMotors(leftMotorSpeed, rightMotorSpeed);
    }
    if (msg[0] == 'B' || msg[0] == 'b')
    {
        // Beacon
        lastBeaconTime = millis();
    }
  }
  if (millis() - lastBeaconTime > 1000 && (leftMotorSpeed != 0 || rightMotorSpeed != 0))
  {
      leftMotorSpeed = 0;
      rightMotorSpeed = 0;
      setMotors(leftMotorSpeed, rightMotorSpeed);
      Serial.println("Beacon timeout exceeded.  Motors off.");
  }

}

// The ISR will be called every 5 milliseconds
ISR(TIMER1_COMPA_vect)
{
  // calculate the angle of inclination
  accAngle = atan2(accY, accZ)*RAD_TO_DEG;
  gyroRate = map(gyroX, -32768, 32767, -250, 250);
  gyroAngle = (float)gyroRate*sampleTime;  
  currentAngle = 0.9934*(prevAngle + gyroAngle) + 0.0066*(accAngle);
  
  error = currentAngle - targetAngle;
  errorSum = errorSum + error;  
  errorSum = constrain(errorSum, -300, 300);

  //calculate output from P, I and D values
  //motorPower = Kp*(error) + Ki*(errorSum)*sampleTime + Kd*(currentAngle-prevAngle)/sampleTime;
  prevAngle = currentAngle;

  // toggle the led on pin13 every second
  count++;
  if(count == 200)  {
    count = 0;
    digitalWrite(13, !digitalRead(13));
  }
}
