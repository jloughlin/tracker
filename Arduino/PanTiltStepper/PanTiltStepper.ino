#include <AccelStepper.h>


#define TILT_ENABLE_PIN 2
#define TILT_DIR_PIN 3
#define TILT_STEP_PIN 4

#define PAN_ENABLE_PIN 5
#define PAN_DIR_PIN 6
#define PAN_STEP_PIN 7

AccelStepper panStepper(AccelStepper::DRIVER, PAN_STEP_PIN, PAN_DIR_PIN);
AccelStepper tiltStepper(AccelStepper::DRIVER, TILT_STEP_PIN, TILT_DIR_PIN);

#define TILT_LIMIT_SW_PIN 12
#define PAN_LIMIT_SW_PIN 11

#define NORMAL_SPEED 4000 //4000
#define NORMAL_ACCEL_RATE 2000 //8000
#define STOP_ACCEL_RATE 100000
#define MIN_POS 10
#define MAX_POS 1590

float Kp = 3.0;
float Ki = 0.0; 
float Kd = 0.0;
#define sampleTime  0.05

int errorX = 0;
int errorY = 0;
int speedX = 0;
int speedY = 0;

void init_PID()
{  
  // initialize Timer1
  cli();          // disable global interrupts
  TCCR1A = 0;     // set entire TCCR1A register to 0
  TCCR1B = 0;     // same for TCCR1B    
  // set compare match register to set sample time 5ms
  OCR1A = 9999; //9999;    
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS11 bit for prescaling by 8
  TCCR1B |= (1 << CS11);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  sei();          // enable global interrupts
}

int count = 0;

int errorSumX = 0;
int errorSumY = 0;
int prevErrorX = 0;
int prevErrorY = 0;

// The ISR will be called every 5 milliseconds
ISR(TIMER1_COMPA_vect)
{
  errorSumX = errorSumX + errorX;
  errorSumY = errorSumY + errorY;
  speedX = errorX * Kp + Ki*(errorSumX)*sampleTime + Kd*(errorX-prevErrorX)/sampleTime;
  speedY = errorY * Kp + Ki*(errorSumY)*sampleTime + Kd*(errorY-prevErrorY)/sampleTime;
  prevErrorX = errorX;
  prevErrorY = errorY;
  
  if (errorX == 0 || abs(errorX) > 1)
  {
     panStepper.setSpeed(speedX);
  }
  if (errorY == 0 || abs(errorY) > 1)
  {
    tiltStepper.setSpeed(speedY);
  }


  // toggle the led on pin13 every second
  count++;
  if(count == 20)  {
    count = 0;
    digitalWrite(13, !digitalRead(13));
  }
  panStepper.runSpeed();
  tiltStepper.runSpeed();
}

void setup()
{
   panStepper.setMaxSpeed(NORMAL_SPEED);
   panStepper.setSpeed(0);
   panStepper.setAcceleration(NORMAL_ACCEL_RATE);

   tiltStepper.setMaxSpeed(NORMAL_SPEED);
   tiltStepper.setSpeed(0);
   tiltStepper.setAcceleration(NORMAL_ACCEL_RATE);

   pinMode(TILT_LIMIT_SW_PIN, INPUT_PULLUP);
   pinMode(PAN_LIMIT_SW_PIN, INPUT_PULLUP);

   pinMode(PAN_ENABLE_PIN, OUTPUT);
   digitalWrite(PAN_ENABLE_PIN, LOW); // ENABLE: LOW   DISABLE: HIGH
   
   pinMode(TILT_ENABLE_PIN, OUTPUT);
   digitalWrite(TILT_ENABLE_PIN, LOW); // ENABLE: LOW   DISABLE: HIGH
   
   Serial.begin(115200);
   
   init_PID();
}


int GetSerialMsg(char* msg)
{
  int idx = 0;
  while(Serial.available())
  {
     panStepper.runSpeed();
     tiltStepper.runSpeed();
     char ch = Serial.read();
     if (ch == '\r' || ch == '\n')
     {
       msg[idx] = '\0';
       //Serial.flush();
       break;
     }
     msg[idx++] = ch;
     for (int n = 0; n < 10; n++)
     {
      panStepper.runSpeed();
      tiltStepper.runSpeed();
     }
//     delay(10);
  }
  return idx;
}


long lastMillis = 0;
void loop()
{
  static long lastMillis = 0;
  char msg[256] = {0};

  GetSerialMsg(msg);

  if (strlen(msg) > 0) 
  {
      if (msg[0] == 'X' || msg[0] == 'x')
      {
          errorX = atoi(msg + 1);
      }
      if (msg[0] == 'Y' || msg[0] == 'y')
      {
          errorY = atoi(msg + 1);
      }
  }
  panStepper.runSpeed();
  tiltStepper.runSpeed();

  if (millis() - lastMillis > 200)
  {
      sprintf(msg, "P:%ld,%ld\n", panStepper.currentPosition(), tiltStepper.currentPosition());
      Serial.print(msg);
      lastMillis = millis();
  }

  
//  Serial.print("x speed: ");
//  Serial.print(speedX);
//  Serial.print("   y speed: ");
//  Serial.println(speedY);
}
