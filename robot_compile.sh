#!/bin/bash
#
# This script will tun the protoc compiler on robot.proto, then copy the proto file to the machine that runs
# controller, and runs the compiler there too.  This simplifies the process of making changes to the
# proto file and propogating them to the client machine(s)
#
python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. robot.proto
#scp tracker.proto robot:tracker
#ssh robot "cd tracker && . env/bin/activate && python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. robot.proto"