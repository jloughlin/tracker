#import os
import signal
import subprocess
import cv2

from concurrent import futures
import grpc
import tracker_pb2_grpc
import tracker_pb2

import depthai as dai
import numpy as np
from datetime import datetime
from pathlib import Path
import serial
import time
from threading import Thread, Lock
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials
import os
import io
import math
import subprocess
import sys
import base64
import argparse
import json
import configparser
import psutil


useIMU = False
catMode = False
mouseDown = False
trackerOn = False
setLevel = False
webMode = False
frame = None
outFrame = None
frameReady = False
bkg = cv2.imread("target.jpg")
rVvalues = None
targetRange = 0
pitch = 0
roll = 0
yaw = 0
panPosition = 0
tiltPosition = 0
panPositionHome = 0
tiltPositionHome = 0
accelValues = None
relativeVelocity = 0
cpuPercent = 0.0
netThroughput = 0.0
avgIntensity = 0.0

print("Parsing args")
parser = argparse.ArgumentParser()
parser.add_argument('-w', action='store_true',
                    help='Web mode.  No local display.')
parser.add_argument('-c', action='store_true',
                    help='Cat mode.  Detect cats instead of humans.')
args = parser.parse_args()
webMode = args.w
catMode = args.c

mutex = Lock()

def SetMotorSpeeds(panOffset, tiltOffset):
    panCommand = "X" + str(int(panOffset)) + "\r"
    #print("Sending Pan command: " + panCommand)
    Serial.write(panCommand.encode())
    tiltCommand = "Y" + str(int(tiltOffset)) + "\r"
    #print("Sending Tilt command: " + tiltCommand)
    Serial.write(tiltCommand.encode())

exitThread= False
centerX = 0
centerY = 0
def panTiltThread(arg):
    global trackerOn
    global targetX
    global targetY
    global setLevel

    print("Starting PanTiltThread")
    while exitThread == False:
        if (trackerOn == True):
            frameWidthDiv2 = 600 / 2
            frameHeightDiv2 = 600 / 2
            xOffset = int(targetX - frameWidthDiv2) / 2
            yOffset = int(targetY - frameHeightDiv2) / 2

            if not mouseDown:
#                print("X Offset = " + str(xOffset) + " Y Offset = " + str(yOffset))
                SetMotorSpeeds(xOffset, yOffset)
        elif (setLevel == True):
            if abs(panPositionHome - panPosition) < 5:
                if panPositionHome - panPosition < 0:
                    xOffset = -1
                elif panPositionHome - panPosition > 0:
                    xOffset = 1
                else:
                    xOffset = 0
            else:
                xOffset = int((panPositionHome - panPosition) / 5)
            if abs(tiltPositionHome - tiltPosition) < 5:
                if tiltPositionHome - tiltPosition < 0:
                    yOffset = 1
                elif tiltPositionHome - tiltPosition > 0:
                    yOffset = -1
                else:
                    yOffset = 0
            else:
                yOffset = -int((tiltPositionHome - tiltPosition) / 5)
            #print("Would set motors speeds to (" + str(xOffset) + ", " + str(yOffset) + ")")
            SetMotorSpeeds(xOffset, yOffset)

            if xOffset == 0 and yOffset == 0:
                setLevel = False
                SetMotorSpeeds(0,0)

        time.sleep(0.001)

identified_face = None
need_id = False
def faceIdThread(arg):
    global identified_face
    global need_id

    print("Starting faceId thread")

    config = configparser.ConfigParser()
    config.read('config.ini')

    face_key = config["face"]["api_key"]
    face_endpoint = config["face"]["api_endpoint"]

    credentials = CognitiveServicesCredentials(face_key)
    client = FaceClient(face_endpoint, credentials)

    person_groups = client.person_group.list()
    for person_group in person_groups:
        print(person_group)

    failure_count = 0
    while (exitThread == False):
        # buf will be the encoded image
        if identified_face is None and need_id and frame is not None and frame.shape[0] == 600:
            print("FaceId thread:  identifying...", end = '')
            ret,buf = cv2.imencode('.jpg', frame)

            # stream-ify the buffer
            stream = io.BytesIO(buf)

            detected_faces = client.face.detect_with_stream(stream, return_face_id=True, return_face_attributes=['age','gender','smile', 'glasses'], recognition_model="recognition_02")

            detected_face_ids = []
            for face in detected_faces:
                #print(face.face_id)
                #print(face.face_rectangle)
                #print(face.face_attributes)
                detected_face_ids.append(face.face_id)

            if len(detected_face_ids) > 0:
                identified_faces = client.face.identify(detected_face_ids, person_group_id=person_groups[0].person_group_id)
                for face in identified_faces:
                    for candidate in face.candidates:
                        #print(candidate.person_id)

                        person = client.person_group_person.get(person_groups[0].person_group_id, candidate.person_id)
                        identified_face = person.name
                        print("Identified face: " + person.name)
                        failure_count = 0

                if len(identified_faces) == 0:
                    failure_count += 1
                    print("failed (failure count = " + str(failure_count) + ")")
            else:
                failure_count += 1
                print("failed (failure count = " + str(failure_count) + ")")

            if failure_count == 5:
                need_id = False
                failure_count = 0
                print("Failure count reset.  Giving up.")
        time.sleep(5)

def timeDeltaToMilliS(delta) -> float:
    return delta.total_seconds()*1000

def SerialThread(arg):
    global panPosition
    global tiltPosition
    while not exitThread:
        try:
                msg = Serial.read_until().decode("utf-8", "strict")
                if str(msg).startswith('P'):
                    parts = str(msg).strip('P:\n').split(',')
                    if len(parts) == 2:
                        try:
                            panPosition = int(parts[0])
                            tiltPosition = int(parts[1])
                            #print(str(panPosition) + " / " + str(tiltPosition))
                        except:
                            print("Error reading (parts = " + parts[0] + ", " + parts[1] + ")")
        except:
            print("error decoding serial message")
        time.sleep(0.1)

#    global serverProcess
#
#    serverProcess = subprocess.Popen([sys.executable, "server.py"])
#    time.sleep(3)
#    asyncio.run(ConnectWebsocket())
#    while not exitThread:
#        time.sleep(1)

def drawBox(img, left, top, right, bottom, distance):
    width = int((right - left) / 3)
    height = int((bottom - top) / 3)

    if identified_face == "Jeff":
        color = (0,0,255)
        size = 4
        len = 10
    else:
        color = (0,255,0)
        size = 2
        len = 5

    cv2.line(img, (left, top), (left + width, top), color, size)
    cv2.line(img, (right - width, top), (right, top), color, size)
    cv2.line(img, (right, top), (right, top + height), color, size)
    cv2.line(img, (right, bottom - height), (right, bottom), color, size)
    cv2.line(img, (right - width, bottom), (right, bottom), color, size)
    cv2.line(img, (left, bottom), (left + width, bottom), color, size)
    cv2.line(img, (left, bottom - height), (left, bottom), color, size)
    cv2.line(img, (left,top), (left, top + height), color, size)

    halfWidth = int((right - left) / 2)
    halfHeight = int((bottom - top) / 2)
    cv2.line(img, (left + halfWidth, top - len), (left + halfWidth, top + len), color, int(size / 2))
    cv2.line(img, (left + halfWidth, bottom - len), (left + halfWidth, bottom + len), color, int(size / 2))
    cv2.line(img, (left - len, top + halfHeight), (left + len, top + halfHeight), color, int(size / 2))
    cv2.line(img, (right - len, top + halfHeight), (right + len, top + halfHeight), color, int(size / 2))

    cv2.putText(img, "Range: " + str(f'{distance:.2f}') + "m", (left, top - 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color)

    if identified_face is not None:
        cv2.putText(frame, "Target identified: " + identified_face, (left, top - 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color)


    return img

def euler_from_quaternion(w, x, y, z):
    """
    Convert a quaternion into euler angles (roll, pitch, yaw)
    roll is rotation around x in radians (counterclockwise)
    pitch is rotation around y in radians (counterclockwise)
    yaw is rotation around z in radians (counterclockwise)
    """
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)

    return roll_x, pitch_y, yaw_z # in radians


def HandleMouseMove(x, y):
    frameWidthDiv2 = frame.shape[1] / 2
    frameHeightDiv2 = frame.shape[0] / 2
    xOffset = x - frameWidthDiv2
    yOffset = y - frameHeightDiv2
    SetMotorSpeeds(xOffset/4, yOffset/4)


def mouse_handler(event, x, y, flags, data):
    global mouseDown
    global targetX
    global targetY

    if event == cv2.EVENT_LBUTTONDOWN:
        mouseDown = True
        HandleMouseMove(x, y)
    if event == cv2.EVENT_LBUTTONUP:
        mouseDown = False
        SetMotorSpeeds(0,0)
    if mouseDown and event == cv2.EVENT_MOUSEMOVE:
        HandleMouseMove(x, y)

class Tracker(tracker_pb2_grpc.TrackerServicer):

    def SetPanTiltSpeeds(self, request, context):
        #print(f"Setting Pan/Tilt to (left={request.panSpeed}, right={request.tiltSpeed})")
        SetMotorSpeeds(request.panSpeed, request.tiltSpeed)
        return tracker_pb2.PanTiltReply(reply="OK")

    def GetStatus(self, request, context):
        while True:
            yield tracker_pb2.Status(tracking=trackerOn, \
                        targetId=identified_face, \
                        targetRange=targetRange, \
                        pitch=math.degrees(pitch), \
                        roll=math.degrees(roll), \
                        yaw=math.degrees(yaw), \
                        accelX=0, \
                        accelY=0, \
                        accelZ=0, \
                        panPosition=panPositionHome - panPosition, \
                        tiltPosition=tiltPositionHome - tiltPosition, \
                        relativeVelocity=relativeVelocity, \
                        cpuPercent=cpuPercent, \
                        memory=psutil.virtual_memory()[2], \
                        netThroughput=netThroughput, \
                        avgIntensity=avgIntensity)
            time.sleep(0.1)

    def GetFrames(self, request, context):
        while True:
            if frame is not None:
                mutex.acquire()
                img_bytes = cv2.imencode('.jpg', frame)[1].tobytes()
                mutex.release()
            yield tracker_pb2.Frame(data=img_bytes)
            time.sleep(0.05)

    def Track(self, request, context):
        global trackerOn
        global setLevel
        trackerOn = request.trackerState
        setLevel = False
        time.sleep(0.1)
        SetMotorSpeeds(0,0)
        return (tracker_pb2.TrackReply(trackerState=trackerOn))

    def SetHomePos(self, request, context):
        global panPositionHome
        global tiltPositionHome
        panPositionHome = panPosition
        tiltPositionHome = tiltPosition
        return (tracker_pb2.SetHomePosReply(reply="OK"))

    def Home(self, request, context):
        global trackerOn
        global setLevel
        print("Homing device to P: " + str(panPositionHome) + "  T: " + str(tiltPositionHome))
        trackerOn = False
        SetMotorSpeeds(0,0)
        setLevel = True
        return (tracker_pb2.HomeReply(reply="OK"))

    def Stop(self, request, context):
        global trackerOn
        global setLevel
        trackerOn = False
        setLevel = False
        time.sleep(0.1)
        SetMotorSpeeds(0,0)
        print("Emergency Stop triggered")
        return (tracker_pb2.StopReply(reply="OK"))

NN_WIDTH, NN_HEIGHT = 300, 300
VIDEO_WIDTH, VIDEO_HEIGHT = 640, 480

if catMode:
    nn_path = 'models/mobilenet-ssd.blob.sh7cmx7NCE1'
else:
    nn_path = 'models/face-detection-retail-0004.blob.sh7cmx7NCE1'


syncNN = True

# --------------- Pipeline ---------------
#Create pipeline
print("Creating pipeline")
pipeline = dai.Pipeline()

# Define sources and outputs
print("Defining sources")
camRgb = pipeline.create(dai.node.ColorCamera)
spatialDetectionNetwork = pipeline.create(dai.node.MobileNetSpatialDetectionNetwork)
monoLeft = pipeline.create(dai.node.MonoCamera)
monoRight = pipeline.create(dai.node.MonoCamera)
stereo = pipeline.create(dai.node.StereoDepth)

xoutRgb = pipeline.create(dai.node.XLinkOut)
xoutNN = pipeline.create(dai.node.XLinkOut)
xoutBoundingBoxDepthMapping = pipeline.create(dai.node.XLinkOut)
#xoutDepth = pipeline.create(dai.node.XLinkOut)

xoutRgb.setStreamName("rgb")
xoutNN.setStreamName("detections")
xoutBoundingBoxDepthMapping.setStreamName("boundingBoxDepthMapping")
#xoutDepth.setStreamName("depth")

# Properties
print("Setting properties")
camRgb.setPreviewSize(300, 300)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(False)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

# Setting node configs
print("Setting node configs")
stereo.initialConfig.setConfidenceThreshold(255)

spatialDetectionNetwork.setBlobPath(nn_path)
spatialDetectionNetwork.setConfidenceThreshold(0.5)
spatialDetectionNetwork.input.setBlocking(False)
spatialDetectionNetwork.setBoundingBoxScaleFactor(0.3)
spatialDetectionNetwork.setDepthLowerThreshold(100)
spatialDetectionNetwork.setDepthUpperThreshold(5000)

# Configure the IMU
if useIMU:
    imu = pipeline.create(dai.node.IMU)
    xlinkOut = pipeline.create(dai.node.XLinkOut)

    xlinkOut.setStreamName("imu")

    # enable ROTATION_VECTOR at 400 hz rate
    #imu.enableIMUSensor(dai.IMUSensor.ROTATION_VECTOR, 100)
    imu.enableIMUSensor([dai.IMUSensor.ACCELEROMETER_RAW, dai.IMUSensor.ROTATION_VECTOR], 100)
    # above this threshold packets will be sent in batch of X, if the host is not blocked and USB bandwidth is available
    imu.setBatchReportThreshold(1)
    # maximum number of IMU packets in a batch, if it's reached device will block sending until host can receive it
    # if lower or equal to batchReportThreshold then the sending is always blocking on device
    # useful to reduce device's CPU load  and number of lost packets, if CPU load is high on device side due to multiple nodes
    imu.setMaxBatchReports(10)

    # Link plugins IMU -> XLINK
    imu.out.link(xlinkOut.input)



# Pipeline is defined, now we can connect to the device


# Linking
print("Linking")
monoLeft.out.link(stereo.left)
monoRight.out.link(stereo.right)

camRgb.preview.link(spatialDetectionNetwork.input)
if syncNN:
    spatialDetectionNetwork.passthrough.link(xoutRgb.input)
else:
    camRgb.preview.link(xoutRgb.input)

spatialDetectionNetwork.out.link(xoutNN.input)
#spatialDetectionNetwork.boundingBoxMapping.link(xoutBoundingBoxDepthMapping.input)

stereo.depth.link(spatialDetectionNetwork.inputDepth)
#spatialDetectionNetwork.passthroughDepth.link(xoutDepth.input)

print("Initializing serial comms")
Serial = serial.Serial(baudrate=115200)
Serial.port = "/dev/ttyUSB0"
Serial.timeout = 1
Serial.open()
time.sleep(2)

if not webMode:
    cv2.namedWindow("Control",cv2.WINDOW_AUTOSIZE)
    cv2.resizeWindow("Control", 300,300)
    cv2.setMouseCallback("Control", mouse_handler, bkg)

print("Starting background threads")
pan = 0
tilt = 0
ptThread = Thread(target = panTiltThread, args = (0,))
ptThread.start()

faceThread = Thread(target = faceIdThread, args = (0,))
faceThread.start()

serialThread = Thread(target=SerialThread, args = (0,))
serialThread.start()

server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
tracker_pb2_grpc.add_TrackerServicer_to_server(Tracker(), server)
server.add_insecure_port('[::]:50051')
server.start()

targetX = 0
targetY = 0
with dai.Device(pipeline) as device:
    # Output queues will be used to get the rgb frames and nn data from the outputs defined above
    q_cam = device.getOutputQueue("rgb", 4, blocking=False)
    q_nn = device.getOutputQueue(name="detections", maxSize=4, blocking=False)
    #xoutBoundingBoxDepthMappingQueue = device.getOutputQueue(name="boundingBoxDepthMapping", maxSize=4, blocking=False)
    #depthQueue = device.getOutputQueue(name="depth", maxSize=4, blocking=False)
    if useIMU:
        imuQueue = device.getOutputQueue(name="imu", maxSize=50, blocking=False)

    baseTs = None

    start_time = time.time()
    counter = 0
    fps = 0
    layer_info_printed = False
 
    def frameNorm(frame, bbox):
        normVals = np.full(len(bbox), frame.shape[0])
        normVals[::2] = frame.shape[1]
        return (np.clip(np.array(bbox), 0, 1) * normVals).astype(int)

    cX = 0
    cY = 0
    detections = []
    signal.signal(signal.SIGINT, signal.default_int_handler)

    noDetectCount = 0
    lastCycleDetection = False
    boundingBoxHistory = []
    rangeHistory = []
    pitchHistory = []
    relativeVelocityHistory = []
    lastCycleTime = 0
    cpuHistory = []
    prevRangeAvg = 0
    while True:
        try:
            frameLocked = False
            in_frame = q_cam.tryGet()
            #in_depth = depthQueue.tryGet()
            #in_depthBoundingBox = xoutBoundingBoxDepthMappingQueue.tryGet()
            in_nn = q_nn.tryGet()

            if in_frame is not None:
                    mutex.acquire()
                    frameLocked = True
                    frame = in_frame.getCvFrame()
                    frame = cv2.resize(frame, (600,600))

            if in_nn is not None:
                detections = in_nn.detections

    #        if in_depth is not None:
    #            depthFrame = in_depth.getCvFrame()

            totalDetections = 0
            centerX = 0
            centerY = 0
            if frame is not None:
                closestDistance = 9999
                for detection in detections:
                    if not catMode or (catMode and detection.label == 8):
                        if detection.spatialCoordinates.z < closestDistance:
                            closestDistance = detection.spatialCoordinates.z

                for detection in detections:
                    if not catMode or (catMode and (detection.label == 8 or detection.label == 12)):
                        if detection.spatialCoordinates.z == closestDistance:
                            bbox = frameNorm(frame, (detection.xmin, detection.ymin, detection.xmax, detection.ymax))
#                            bbox = np.append(bbox, detection.spatialCoordinates.z / 1000)
                            rangeHistory.append(detection.spatialCoordinates.z / 1000)
                            boundingBoxHistory.append(bbox)
                            if len(boundingBoxHistory) > 10:
                                boundingBoxHistory.pop(0)
                            if len(rangeHistory) > 20:
                                rangeHistory.pop(0)
                            avg = [float(sum(col))/len(col) for col in zip(*boundingBoxHistory)]
                            rangeAvg = sum(rangeHistory) / len(rangeHistory)
                            frame = drawBox(frame, int(avg[0]), int(avg[1]), int(avg[2]), int(avg[3]), rangeAvg)
                            targetRange = rangeAvg
                            centerX += bbox[0] + (bbox[2] - bbox[0]) / 2
                            centerY += bbox[1] + (bbox[3] - bbox[1]) / 2
                            totalDetections += 1
                            if not catMode and lastCycleDetection == False:
                                need_id = True
                            lastCycleDetection = True
                            noDetectCount = 0
                            now = time.time_ns() / 1000000000
                            if lastCycleTime != 0:
                                num = int(len(rangeHistory) / 2)
                                #oldRange = sum(rangeHistory[:num]) / num
                                #newRange = sum(rangeHistory[num:]) / num
                                oldRange = prevRangeAvg
                                newRange = rangeAvg
                                prevRangeAvg = rangeAvg
                                elapsedTime = now - lastCycleTime
                                v = (newRange  - oldRange) / elapsedTime
                                #if abs(v) < 0.05:
                                #    v = 0.0
                                #print(str(oldRange - newRange) + " / " + str(elapsedTime) + " = " + str(v))
                                relativeVelocityHistory.append(v)
                                if len(relativeVelocityHistory) > 50:
                                    relativeVelocityHistory.pop(0)
                                rv = sum(relativeVelocityHistory) / len(relativeVelocityHistory)
                                if abs(rv) < 0.05:
                                    relativeVelocity = 0.0
                                else:
                                    relativeVelocity = rv
                            lastCycleTime = now

                if (trackerOn == True):
                    cv2.putText(frame, "Tracking", (10,20), cv2.FONT_HERSHEY_SIMPLEX, .5, (0,0,255))

                if frameLocked:
                    mutex.release()

                if not webMode:
                    cv2.imshow("Cam", frame)
                    cv2.imshow("Control", bkg)
    #            if depthFrame is not None:
    #                cv2.imshow("Depth", depthFrame)
            if totalDetections == 0:
                noDetectCount += 1
                need_id = False
                lastCycleDetection = False
                targetRange = 0
                relativeVelocity = 0
                targetRange = 0

            if noDetectCount == 30:
                identified_face = None
                noDetectCount = 0

            # Get the IMU data from the device
            if useIMU:
                imuData = imuQueue.get()  # blocking call, will wait until a new data has arrived

                imuPackets = imuData.packets
                for imuPacket in imuPackets:
                    rVvalues = imuPacket.rotationVector

                    rvTs = rVvalues.timestamp.get()
                    if baseTs is None:
                        baseTs = rvTs
                    rvTs = rvTs - baseTs

                    imuF = "{:.06f}"
                    tsF  = "{:.03f}"

                    roll, pitch, yaw = euler_from_quaternion(rVvalues.real, rVvalues.i, rVvalues.j, rVvalues.k)

                    accelValues = imuPacket.acceleroMeter

                    pitch = math.atan2(-accelValues.z, math.sqrt(accelValues.x * accelValues.x + accelValues.y * accelValues.y))
                    roll = math.atan2(accelValues.x, accelValues.y) 
                    pitchHistory.append(pitch)
                    if len(pitchHistory) > 10:
                        pitchHistory.pop(0)
                    pitch = sum(pitchHistory) / len(pitchHistory)
                    #print(f"pitch = {pitch} accelX = {imuF.format(accelValues.x)}  accelY = {imuF.format(accelValues.y)} accelZ = {imuF.format(accelValues.z)}")
                    #print("Roll: " + str(math.degrees(roll)) + "  Pitch: " + str(math.degrees(pitch)) + "  Yaw: " + str(math.degrees(yaw)))

                    #            print(f"Rotation vector timestamp: {tsF.format(timeDeltaToMilliS(rvTs))} ms")
                    #            print(f"Quaternion: i: {imuF.format(rVvalues.i)} j: {imuF.format(rVvalues.j)} "
                    #                f"k: {imuF.format(rVvalues.k)} real: {imuF.format(rVvalues.real)}  levelError:{imuF.format(rVvalues.i + rVvalues.k)}  setLevel: {str(setLevel)}")
                    #            print(f"Accuracy (rad): {imuF.format(rVvalues.rotationVectorAccuracy)}")
                    #        print("trackerOn = " +str(trackerOn) + "   setLevel = " + str(setLevel))

            if totalDetections != 0:
                targetX = centerX / totalDetections
                targetY = centerY / totalDetections
            else:
                targetX = 300
                targetY = 300

            cpuHistory.append(psutil.cpu_percent())
            if len(cpuHistory) > 100:
                cpuHistory.pop(0)
            cpuPercent = sum(cpuHistory) / len(cpuHistory)

            # TODO: Figure out out to calculate somethng that represents the brightneess of the frame
            #       without slowing things down too much
            if frame is not None:            
                avgIntensity = 0 #frame.mean(axis=0).mean(axis=0)

            if not webMode:
                key = cv2.waitKey(1)
                if key == ord('q') or key == 27:
                    SetMotorSpeeds(0,0)
                    break
                elif key == ord(' '):
                    setLevel = False
                    SetMotorSpeeds(0,0)
                    trackerOn = not trackerOn
                    time.sleep(0.1)
                    SetMotorSpeeds(0,0)
                elif key == ord('l'):
                    trackerOn = False
                    setLevel = True
                    SetMotorSpeeds(0,0)
            else:
                time.sleep(0.01)

        except KeyboardInterrupt:
            SetMotorSpeeds(0,0)
            del pipeline
            del device
            exitThread = True
            time.sleep(1)
            SetMotorSpeeds(0,0)
            exit(0)
                            

# The pipeline object should be deleted after exiting the loop. Otherwise device will continue working.
# This is required if you are going to add code after exiting the loop.
SetMotorSpeeds(0,0)
del pipeline
del device
exitThread = True
time.sleep(1)
SetMotorSpeeds(0,0)
