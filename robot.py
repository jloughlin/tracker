import serial
import time
from threading import Thread, Lock
import numpy as np
#import cv2
#import json
import configparser

from concurrent import futures
import grpc
import robot_pb2_grpc
import robot_pb2
import tracker_pb2_grpc
import tracker_pb2

exitThread = False

status = None

#
# Receive messages from the serial port
#
def SerialThread():
    while not exitThread:
        try:
            msg = Serial.read_until().decode("utf-8", "strict")
            if len(msg) > 0:
                print(str(msg))
            if str(msg).startswith('P'):
                parts = str(msg).strip('P:\n').split(',')
                if len(parts) == 2:
                    #panPosition = int(parts[0])
                    #tiltPosition = int(parts[1])
                    print("ok")
        except:
            print("error decoding serial message")
        time.sleep(0.1)

#
# Send a watchdog beacon every 0.5 seconds so the firmware knows we're still connected
def WatchdogThread():
    while not exitThread:
        serialMutex.acquire()
        Serial.write("B\r".encode())
        serialMutex.release()
        time.sleep(.5)

def StatusThread():
    global status
    response = trackerStub.GetStatus(tracker_pb2.StatusRequest())
    for status in response:
        print(status.cpuPercent)

#
# Send a message to the serial port
#    Acquire the mutex lock, send the message with '\r' terminator, release the mutex
#
def sendSerialMsg(msg):
    serialMutex.acquire()
    Serial.write((msg + "\r").encode())
    serialMutex.release()

class Robot(robot_pb2_grpc.RobotServicer):

    def SetMotors(self, request, context):
        print(f"Setting motors to (left={request.left}, right={request.right})")
        msg = "M" + str(request.left) + "," + str(request.right)
        sendSerialMsg(msg)
        return robot_pb2.SetMotorsReply(reply="OK")


#
# Setup
#    Parese the config file
#    Create the serial object, open it
#    Create the mutex
#    Start the threads
#
config = configparser.ConfigParser()
config.read('config.ini')
#robot_socket_addr = config["robot"]["robot_socket_addr"]
#robot_socket_port = int(config["robot"]["robot_socket_port"])
#cam_socket_addr = config["camera"]["camera_socket_addr"]
#cam_socket_port = int(config["camera"]["camera_socket_port"])
serial_port_name = config["robot"]["serial_port_name"]


Serial = serial.Serial(baudrate=115200)
Serial.port = serial_port_name
Serial.timeout = 1
Serial.rtscts = True
Serial.open()

# Not sure why, but have to sleep for a second or so after opening the serial port
# before we can use it...
time.sleep(1)

img = None

serialMutex = Lock()

# Start serial read thread
serialThread = Thread(target=SerialThread, name="SerialThead")
serialThread.start()

# Start watchdog thread
watchdogThread = Thread(target=WatchdogThread, name="WatchdogThread")
watchdogThread.start()

trackerChannel =  grpc.insecure_channel('192.168.1.189:50051')
trackerStub = tracker_pb2_grpc.TrackerStub(trackerChannel)

# Start the gRPC server
server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
robot_pb2_grpc.add_RobotServicer_to_server(Robot(), server)
server.add_insecure_port('[::]:50051')
server.start()

# Start the status thread
statusThread = Thread(target=StatusThread, name="StatusThread")
statusThread.start()


#
# Main loop
#
while True:
    #sendSerialMsg("m100,200")
    #time.sleep(5)
    #sendSerialMsg("m200,100")
    time.sleep(5)
