#!/bin/bash
#
# This script will tun the protoc compiler on tracker.proto, then copy the proto file to the machine that runs
# controller and robot, and runs the compiler there too.  This simplifies the process of making changes to the
# proto file and propogating them to the client machine(s)
#
python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. tracker.proto
scp tracker.proto bart:tracker
ssh bart "cd tracker && . env/bin/activate && python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. tracker.proto"