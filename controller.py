from __future__ import print_function
import cv2
import numpy as np
import matplotlib.pyplot as plt
import time
import json
import math
import queue
from threading import Thread, Lock
import pygame
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.locals import *
import configparser

import grpc
import robot_pb2
import robot_pb2_grpc
import tracker_pb2
import tracker_pb2_grpc


status = None
exitThread = False
trackerOn = False
btn_down = False
btnDragX = None
btnDragY = None

def CheckShutdown():
    if exitThread:
        reactor.stop()

def resizewin(width, height):
    """
    For resizing window
    """
    if height == 0:
        height = 1
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, width/height, 0.1, 600.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def init():
    glShadeModel(GL_SMOOTH)
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LEQUAL)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

class Button:
    def __init__(self, _x, _y, _width, _height, _label, _textColor, _color, _onClick):
        self.X = _x
        self.Y = _y
        self.Width = _width
        self.Height = _height
        self.Label = _label
        self.TextColor = _textColor
        self.Color = _color
        self.OnClick = _onClick

    def draw(self):
        #cx = -np.interp(self.X, [0, 1600], [-17.325, 17.325])
        #cy = -np.interp(self.Y, [0, 900], [-8.15, 8.15])
        #cw = np.interp(self.Width, [0, 1600], [0, 34.65])
        #ch = np.interp(self.Height, [0, 900], [0, 16.3])
        cx = -np.interp(self.X, [0, 1600], [-14.725, 14.725])
        cy = -np.interp(self.Y, [0, 900], [-8.275, 8.275])
        cw = np.interp(self.Width, [0, 1600], [0, 29.45])
        ch = np.interp(self.Height, [0, 900], [0, 16.55])
        glBegin(GL_QUADS)
        glColor3f(self.Color[0] / 255, self.Color[1] / 255, self.Color[2] / 255)
        glVertex3f(6, cy, cx)
        glVertex3f(6, cy, cx - cw)
        glVertex3f(6, cy - ch, cx - cw)
        glVertex3f(6, cy - ch, cx)
        glEnd()

        txtWidth = len(self.Label) * 7
        txtOffset = (self.Width - txtWidth) / 2
        cTxtOffset = np.interp(txtOffset, [0,1600], [0, 29.45])
        drawText((6, cy - (ch / 2) - .1, cx - cTxtOffset), self.Label, 12, bkgColor=self.Color)

    def HitTest(self, x, y):
        return x > self.X and x < self.X + self.Width and y > self.Y and y < self.Y + self.Height


def draw(w, nx, ny, nz):
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()

    glTranslatef(0, 0, -20) ######

    gluLookAt(6,0,0,0,0,0,0,1,0)
    #glLoadIdentity()
    #glTranslatef(0, 0, -10)

    # Draw the camera fram on the OpenGL canvas
    if img is not None:
        newimg = cv2.flip(img, 0)
        newimg = cv2.cvtColor(newimg,cv2.COLOR_BGR2RGB)
        glRasterPos3d(0, -3.7, 19.0)
        glDrawPixels(600,600, GL_RGB,GL_UNSIGNED_BYTE, newimg)

    [yaw, pitch , roll] = quat_to_ypr([w, nx, ny, nz])
    if (yaw, pitch, roll) == (-1, -1, -1):
        drawText((0, -4.5, 18), "<Gimble lock>", 16)
    elif (yaw, pitch,roll) != (0, 0, 0):
        drawText((0, -4.5, 18), "Yaw: %f, Pitch: %f, Roll: %f" %(yaw, pitch, roll), 16)

    if (w,nx,ny,nz) != (0,0,0,0):
       drawText((0, 1, 2), "Orientation:",16)

    if status is not None:
        # Below the cam frame
        if (status.accelX, status.accelY, status.accelZ) != (0,0,0):
            drawText((0, -5, 18), "accel X: " + str(status.accelX), 16)
            drawText((0, -5.5, 18), "accel Y: " + str(status.accelY), 16)
            drawText((0, -6, 18), "accel Z: " + str(status.accelZ), 16)
        drawText((0, -7.0, 18), "   CPU Load: " + "{0:.3g}%".format(status.cpuPercent), 16)
        drawText((0, -7.5, 18), "Memory Load: " + "{0:.3g}%".format(status.memory), 16)


        # Right of the cam frame
        #drawText((0, 10, 3.5),  " Connection Status: " + status["status"], 16)
        drawText((0, 9.5, 3.5), "      Target Range: " + "{0:.2g}".format(status.targetRange) + " m", 16)
        drawText((0, 9.0, 3.5), " Relative velocity: " + "{0:.2g}".format(status.relativeVelocity) + " m/s", 16)
        drawText((0, 8.5, 3.5), "         Target Id: " + status.targetId, 16)
        drawText((0, 7.5, 3.5), "      Pan Position: " + str(status.panPosition), 16)
        drawText((0, 7.0, 3.5), "     Tilt Position: " + str(status.tiltPosition), 16)

    glColor3f(1.0,0.0,0.0)
    glPointSize(5.0)
    glBegin(GL_LINES)
    glVertex3f(6, 7.5, 14.725)
    glVertex3f(6, 8, 14.725)
    glEnd()


    # Draw the pan/tilt crosshair
    glColor3f(1.0,0.0,0.0)
    glPointSize(5.0)
    glBegin(GL_LINES)
    glVertex3f(6, 5, -5)
    glVertex3f(6, 5, -10)
    glEnd()
    glBegin(GL_LINES)
    glVertex3f(6, 2.5, -7.5)
    glVertex3f(6, 7.5, -7.5)
    glEnd()

    if btn_down and btnDragX is not None and btnDragY is not None and btnDragX > 1070 and btnDragX < 1345  and btnDragY > 45 and btnDragY < 315:
        cx = -np.interp(btnDragX, [0, 1600], [-14.725, 14.725])
        cy = -np.interp(btnDragY, [0, 900], [-8.275, 8.275])
    else:
        cx = -7.5
        cy = 5.0

    r = 0.25
    num_segments = 10.0
    glBegin(GL_TRIANGLE_FAN)
    for ii in range(0, int(num_segments)):
        theta = 2.0 * 3.1415926 * float(ii) / float(num_segments)
        x = r * math.cos(theta)
        y = r * math.sin(theta)
        glVertex3f(6, y + cy, x + cx)
    glEnd()

    glBegin(GL_LINES)
    glVertex3f(6, cy, cx)
    glVertex3f(6, 5.0, -7.5)
    glEnd()


    # Draw the motor control crosshair
    glColor3f(1.0,0.0,0.0)
    glPointSize(5.0)
    glBegin(GL_LINES)
    glVertex3f(6, 2.4, 3.6)
    glVertex3f(6, 2.4, 14.6)
    glEnd()
    glBegin(GL_LINES)
    glVertex3f(6, -2.8, 9.0)
    glVertex3f(6, 8.1, 9.0)
    glEnd()


    # Draw the buttons
    for button in Buttons:
        button.draw()

    if (w,nx,ny,nz) != (0,0,0,0):
        glRotatef(2 * math.acos(w) * 180.00/math.pi, -1 * nx, nz, ny)

        length = 1
        width = 0.3
        depth = 0.2
        
        glBegin(GL_QUADS)
        glColor3f(0.0, 1.0, 0.0)
        glVertex3f(width, depth, -length)
        glVertex3f(-width, depth, -length)
        glVertex3f(-width, depth, length)
        glVertex3f(width, depth, length)

        glColor3f(1.0, 0.5, 0.0)
        glVertex3f(width, -depth, length)
        glVertex3f(-width, -depth, length)
        glVertex3f(-width, -depth, -length)
        glVertex3f(width, -depth, -length)

        glColor3f(1.0, 0.0, 0.0)
        glVertex3f(width, depth, length)
        glVertex3f(-width, depth, length)
        glVertex3f(-width, -depth, length)
        glVertex3f(width, -depth, length)

        glColor3f(1.0, 1.0, 0.0)
        glVertex3f(width, -depth, -length)
        glVertex3f(-width, -depth, -length)
        glVertex3f(-width, depth, -length)
        glVertex3f(width, depth, -length)

        glColor3f(0.0, 0.0, 1.0)
        glVertex3f(-width, depth, length)
        glVertex3f(-width, depth, -length)
        glVertex3f(-width, -depth, -length)
        glVertex3f(-width, -depth, length)

        glColor3f(1.0, 0.0, 1.0)
        glVertex3f(width, depth, -length)
        glVertex3f(width, depth, length)
        glVertex3f(width, -depth, length)
        glVertex3f(width, -depth, -length)
        glEnd()
    


def drawText(position, textString, size, textColor=(255,255,255.255), bkgColor=(0,0,0,255)):
    font = pygame.font.SysFont("Consolas", size, True)
    textSurface = font.render(textString, True, textColor, bkgColor)
    textData = pygame.image.tostring(textSurface, "RGBA", True)
    glRasterPos3d(*position)
    glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)

def quat_to_ypr(q):
    if (q == [0,0,0,0]):
        return [0,0,0]
    try:
        yaw   = math.atan2(2.0 * (q[1] * q[2] + q[0] * q[3]), q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3])
        pitch = -math.asin(2.0 * (q[1] * q[3] - q[0] * q[2]))
        roll  = math.atan2(2.0 * (q[0] * q[1] + q[2] * q[3]), q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3])
        pitch *= 180.0 / math.pi
        yaw   *= 180.0 / math.pi
        yaw   -= -0.13  # Declination at Chandrapur, Maharashtra is - 0 degress 13 min
        roll  *= 180.0 / math.pi
    except:
        yaw, pitch, roll = -1, -1, -1
    return [yaw, pitch, roll]


def TrackerButtonClicked():
    response = trackerStub.Track(request=tracker_pb2.TrackRequest(trackerState = not status.tracking))

def SetHomePosButtonClicked():
    response = trackerStub.SetHomePos(request=tracker_pb2.SetHomePosRequest())

def HomeButtonClicked():
    response = trackerStub.Home(request=tracker_pb2.HomeRequest())

def StopButtonClicked():
    response = trackerStub.Stop(request=tracker_pb2.StopRequest())

def StatusThread():
    global status
    response = trackerStub.GetStatus(tracker_pb2.StatusRequest())
    for status in response:
        if status.tracking and trackerButton is not None:
            trackerButton.Color = (51, 128, 51)
        elif trackerButton is not None:
            trackerButton.Color = (255, 0, 0)

def FrameThread():
    global img
    frames = trackerStub.GetFrames(tracker_pb2.FrameRequest())
    for frame in frames:
        nparr = np.frombuffer(frame.data, np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

#
# Setup
#
# Parse config file
#config = configparser.ConfigParser()
#config.read('config.ini')
#robot_socket_addr = config["robot"]["robot_socket_addr"]
#robot_socket_port = int(config["robot"]["robot_socket_port"])
#cam_socket_addr = config["camera"]["camera_socket_addr"]
#cam_socket_port = int(config["camera"]["camera_socket_port"])

# Create the buttons
Buttons = []
trackerButton = Button(1453,20,80,30,"Tracker", (255, 255, 255), (51, 128, 15), TrackerButtonClicked)
Buttons.append(trackerButton)
setHomePosButton = Button(1453,55,80,30,"Set Home", (255, 255, 255), (51, 128, 51), SetHomePosButtonClicked)
Buttons.append(setHomePosButton)
homeButton = Button(1453,90,80,30,"Home", (255, 255, 255), (51, 128, 51), HomeButtonClicked)
Buttons.append(homeButton)
stopButton = Button(1453,125,80,30,"Stop", (255, 255, 255), (51, 128, 51), StopButtonClicked)
Buttons.append(stopButton)

# Initialize the OpenGL window
video_flags = OPENGL | DOUBLEBUF
pygame.init()
screen = pygame.display.set_mode((1600, 900), video_flags)
pygame.display.set_caption("Telemetry / Control")
resizewin(1600, 900)
init()
frames = 0
ticks = pygame.time.get_ticks()

img = None

# Create the gRPC stubs
robotChannel =  grpc.insecure_channel('localhost:50051')
robotStub = robot_pb2_grpc.RobotStub(robotChannel)
trackerChannel =  grpc.insecure_channel('192.168.1.189:50051')
trackerStub = tracker_pb2_grpc.TrackerStub(trackerChannel)

lastCommandTime = 0

# Start the status thread
statusThread = Thread(target=StatusThread, name="StatusThread")
statusThread.start()

# Start the frame thread
frameThread = Thread(target=FrameThread, name="FrameThread")
frameThread.start()

#
# Main loop
#
while True:
    if status is not None:
        event = pygame.event.poll()
        #if "Quaternion_real" in status:
        #    [w, nx, ny, nz] = status["Quaternion_real"], status["Quaternion_i"], status["Quaternion_j"], status["Quaternion_k"]
        #else:
        [w, nx, ny, nz] = 0, 0, 0, 0
        draw(w, nx, ny, nz)    
        pygame.display.flip()
        frames += 1

        if event.type == pygame.MOUSEBUTTONUP and btn_down:
            btn_down = False
            btnDragX = None
            btnDragY = None
            time.sleep(0.1)
            req = tracker_pb2.PanTiltRequest(panSpeed=0, tiltSpeed=0)
            response = trackerStub.SetPanTiltSpeeds(request=req)
            time.sleep(0.1)
            req = robot_pb2.SetMotorsRequest(left=0, right=0)
            response = robotStub.SetMotors(request=req)

        if event.type == pygame.MOUSEBUTTONDOWN or (event.type == pygame.MOUSEMOTION and btn_down):
            pos = pygame.mouse.get_pos()
            x, y = pos

            #  TODO: Fix this.  It should stop the motors if you slide off the motor control image area with the button down
            #if btn_down and x > 607 and x < 1200 or y > 607:
            #    btn_down = False
            #    commandQueue.put("M0,0")

            # Mouse button down in Motor Control region
            if x < 607 and y < 607:
                btn_down = True

                #adjust x and y for screen position
                x -= 312 
                y -= 321

                leftMotorSpeed = int(-y / 2)
                rightMotorSpeed = int(-y / 2)

                leftMotorSpeed += int(x / 2)
                rightMotorSpeed -= int(x / 2)

                #print(f"x = {x}  y = {y}")

                if time.time() - lastCommandTime > 0.1:
                    req = robot_pb2.SetMotorsRequest(left=leftMotorSpeed, right=rightMotorSpeed)
                    response = robotStub.SetMotors(request=req)
                    lastCommandTime = time.time()
                #print("Client received: " + response.reply)

            # Mouse down in Pan/Tilt region
            elif x > 1070 and x < 1345  and y > 45 and y < 315:
                btn_down = True
                btnDragX = x
                btnDragY = y

                x -= 1208
                y -= 178
                x = int(x / 2)
                y = int(y / 2)
                #print(f"x = {x}, y={y}")
                if time.time() - lastCommandTime > 0.1:
                    req = tracker_pb2.PanTiltRequest(panSpeed=x, tiltSpeed=y)
                    response = trackerStub.SetPanTiltSpeeds(request=req)

            #print(f"x = {x}, y={y}")

            # Process button clicks
            for button in Buttons:
                if button.HitTest(x, y):
                    button.OnClick()
 