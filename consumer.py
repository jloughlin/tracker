import asyncio
import logging
import websockets
from websockets import WebSocketClientProtocol
import cv2
import numpy as np
import base64

logging.basicConfig(level=logging.INFO)

async def consumer_handler(websocket: WebSocketClientProtocol) -> None:
    async for message in websocket:
        log_message(message)

async def consume(hostname:str, port: int) -> None:
    websocket_resource_url = f"ws://{hostname}:{port}"
    async with websockets.connect(websocket_resource_url) as websocket:
        await consumer_handler(websocket)

def log_message(message: bytes) -> None:
#    logging.info(f"Message: {message}")
    #b64decoded = base64.b64decode(message)
    jpg_as_np = np.frombuffer(message, dtype=np.uint8)
    img = cv2.imdecode(jpg_as_np, cv2.IMREAD_COLOR)
    if img is not None:
        cv2.imshow("Received", img)
        cv2.waitKey(1)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(consume(hostname='192.168.1.189', port=4000))
    loop.run_forever()
